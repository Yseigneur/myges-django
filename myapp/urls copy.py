from django.urls import path,include
from myapp.views import  classCreate, classDelete, classUpdate, promotionCreate, promotionDelete, promotionUpdate, subjectCreate, subjectDelete, subjectUpdate, classList, promotionList, subjectList
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    #path('class/<int:class_id>', views.classe, name='class'),
    path('login',views.login, name='login'),
    path('create_user',views.createUser, name='create_user'),
   # path('profil',views.profil, name='profil'),
    path('class/', classList.as_view(), name='class-list'),
    path('promotion/', promotionList.as_view(), name='promotion-list'),
    path('subject/', subjectList.as_view(), name='subject-list'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('class/add/', classCreate.as_view(), name='class-add'),
    path('class/<int:pk>/', classUpdate.as_view(), name='class-update'),
    path('class/<int:pk>/delete/', classDelete.as_view(), name='class-delete'),
   # path('student/<int:promotion>/', studentPromotionList.as_view(), name='student-promotion'),
    path('promotion/add/', promotionCreate.as_view(), name='promotion-add'),
    path('promotion/<int:pk>/', promotionUpdate.as_view(), name='promotion-update'),
    path('promotion/<int:pk>/delete/', promotionDelete.as_view(), name='promotion-delete'),
    path('subject/add/', subjectCreate.as_view(), name='subject-add'),
    path('subject/<int:pk>/', subjectUpdate.as_view(), name='subject-update'),
    path('subject/<int:pk>/delete/', subjectDelete.as_view(), name='subject-delete'),
    #path('create_class',views.createClass, name='create_class'),
]
from django import forms
from django.contrib.auth.models import User
from .models import Faculty, Profil

USER_ROLES = [
    ('student', 'Student'),
    ('professeur','Professeur')
]

class CreateUserForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100)
    email = forms.EmailField(label='Email', max_length=100)
    password = forms.CharField(label='Password', max_length=100,widget=forms.PasswordInput)
    firstName = forms.CharField(label='First Name',max_length=50)
    lastName = forms.CharField(label='Last Name',max_length=50)
    droit = forms.ChoiceField(required=True,choices=USER_ROLES)

class CreateProfilForm(forms.Form):
    image = forms.ImageField(label='Image', max_length=100)
    user = forms.ModelChoiceField(queryset=User.objects.all(), empty_label="(Nothing)")

class CreateClassForm(forms.Form):
    name = forms.CharField(label='Name', max_length=100)
    capacity = forms.IntegerField(label='Capacity')
    faculty = forms.ModelChoiceField(queryset=Faculty.objects.all(), empty_label="(Nothing)")
    student = forms.ModelMultipleChoiceField(queryset=User.objects.all())

from django.http import HttpResponse,HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User,Group
from .models import Class, Promotion, Subject, Profil
from .forms import CreateUserForm, CreateClassForm, CreateProfilForm
from django.views.generic.edit import FormView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.views.generic import ListView

def is_student(user):
    return user.groups.filter(name='Student').exists()
def is_coordinator(user):
    return user.groups.filter(name='Coordinator').exists()
    
def index(request):
    classes = Class.objects.all()
    template = loader.get_template('myapp/index.html')
    group = Group.objects.get(id=2)
    students = group.user_set.all().count()
    currentUser = request.user
    context = {
        'classes':classes,
        'currentUser':currentUser,
        'students':students,
        'is_student': is_student(currentUser),
        'is_coordinator': is_coordinator(currentUser)
    }
    return HttpResponse(template.render(context, request))

def classe(request, class_id):
   class1 = Class.objects.get(pk=class_id)
   template = loader.get_template('myapp/class.html')
   context = {
       'class': class1
   }
   return HttpResponse(template.render(context, request))

def login(request):
    template = loader.get_template('myapp/templates/registration/login.html')
    return HttpResponse(template.render(request))

def createUser(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'])
            user.first_name = form.cleaned_data['firstName']
            user.last_name = form.cleaned_data['lastName']
            #todo : add group !!!! important
            if form.cleaned_data['droit'] == 'student':
                studentgroup = Group.objects.get(name="Student")
                studentgroup.user_set.add(user)
            if form.cleaned_data['droit'] == 'professeur':
                studentgroup = Group.objects.get(name="Professeur")
                studentgroup.user_set.add(user)
            user.save()
            return HttpResponseRedirect('/')
    else:
        form = CreateUserForm()
    template = loader.get_template('myapp/createUser.html')
    context = {
        'form':form
    }
    return HttpResponse(template.render(context, request))

"""def profil(request):
    if request.method == 'POST':
        form = CreateProfilForm(request.POST)
        if form.is_valid():
            profil = Profil.objects.profil(form.cleaned_data['user'])
            profil.image = form.cleaned_data['image']
            profil.save()
            return HttpResponseRedirect('/')
    else:
        form = CreateProfilForm()
    template = loader.get_template('myapp/profil.html')
    context = {
        'form':form
    }
    return HttpResponse(template.render(context, request))"""

"""def createClass(request):
    if request.method == 'POST':
        form = CreateClassForm(request.POST)
        if form.is_valid():
            class2 = Class.objects.create_class(form.cleaned_data['name'], form.cleaned_data['capacity'], form.cleaned_data['student'])
            class2.name = form.cleaned_data['name']
            class2.capacity = form.cleaned_data['capacity']
            class2.student = form.cleaned_data['student']
           
            class2.save()
            return HttpResponseRedirect('/')
    else:
        form = CreateClassForm()
    template = loader.get_template('myapp/createClass.html')
    context = {
        'form':form
    }
    return HttpResponse(template.render(context, request))
"""
"""class CreateClass(FormView):
    template_name = 'myapp/createClass.html'
    form_class = CreateClassForm
    success_url = '/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        return super().form_valid(form)
"""
class classCreate(CreateView):
    model = Class
    fields = ['name','capacity', 'faculty', 'student']
    template_name = 'myapp/createClass.html'
    success_url = '/'

class classUpdate(UpdateView):
    model = Class
    fields = ['name','capacity', 'faculty', 'student']
    template_name = 'myapp/createClass.html'
    success_url = '/'
class classDelete(DeleteView):
    model = Class 
    template_name = 'myapp/deleteClass.html'
    success_url = '/'
   # success_url = reverse_lazy('class-list')

class promotionCreate(CreateView):
    model = Promotion
    fields = ['year', 'student']
    template_name = 'myapp/createClass.html'
    success_url = '/'

class promotionUpdate(UpdateView):
    model = Promotion
    fields = ['year', 'student']
    template_name = 'myapp/createClass.html'
    success_url = '/'
class promotionDelete(DeleteView):
    model = Promotion 
    template_name = 'myapp/deleteClass.html'
    success_url = '/'
   # success_url = reverse_lazy('class-list')

class subjectCreate(CreateView):
    model = Subject
    fields = ['name', 'prof','promo']
    template_name = 'myapp/createClass.html'
    success_url = '/'

class subjectUpdate(UpdateView):
    model = Subject
    fields = ['name', 'prof', 'promo']
    template_name = 'myapp/createClass.html'
    success_url = '/'
class subjectDelete(DeleteView):
    model = Subject 
    template_name = 'myapp/deleteClass.html'
    success_url = '/'
   # success_url = reverse_lazy('class-list')

class classList(ListView):
    model = Class
    template_name = 'myapp/classList.html'

class promotionList(ListView):
    model = Promotion
    template_name = 'myapp/promotionList.html'

class subjectList(ListView):
    model = Subject
    template_name = 'myapp/subjectList.html'

#class studentPromotionList(ListView):
#    model = User
#   template_name = 'myapp/classList.html'

# Generated by Django 3.1.7 on 2021-04-01 13:12

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('myapp', '0005_auto_20210331_2153'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subject',
            name='subject',
        ),
        migrations.AddField(
            model_name='subject',
            name='prof',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]

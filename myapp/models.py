from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Promotion(models.Model):
    year = models.IntegerField(default=2021)
    student = models.ManyToManyField(User,  )
    def get_absolute_url(self):
        return reverse('promotion-detail', kwargs={'pk': self.pk})
    def __str__(self):
        return str(self.year)

class Faculty(models.Model):
    name = models.CharField(max_length=64)
    def __str__(self):
        return self.name
    

class Class(models.Model):
    name = models.CharField(max_length=64)
    capacity = models.IntegerField(default=10)
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE, default=1)
    student = models.ManyToManyField(User,  )
    class Meta:
        ordering = ["name"]
    def get_absolute_url(self):
        return reverse('class-detail', kwargs={'pk': self.pk})
    def __str__(self):
        return str(self.name)

class Subject(models.Model):
    name = models.CharField(max_length=64)
    image = models.ImageField( max_length=100, null = True)
    prof = models.ManyToManyField(User, )
    promo = models.ManyToManyField(Promotion, )
    #sub = models.TextChoices('sub', 'PYTHON PHP JAVASCRIPT')
    #subject = models.CharField(blank=True, choices=sub.choices, max_length=64)
    class Meta:
        ordering = ["name"]
    def get_absolute_url(self):
        return reverse('promotion-detail', kwargs={'pk': self.pk})
    def __str__(self):
        return self.name
        
class Profil(models.Model):
    
    image = models.ImageField( max_length=100, null = True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    #sub = models.TextChoices('sub', 'PYTHON PHP JAVASCRIPT')
    #subject = models.CharField(blank=True, choices=sub.choices, max_length=64)
   
    def __str__(self):
        return self.name
# filiere, plannig, trombi, 
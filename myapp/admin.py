from django.contrib import admin
from .models import Promotion,Class,Subject, Faculty, Profil
# Register your models here.
admin.site.register(Promotion)
admin.site.register(Class)
admin.site.register(Subject)
admin.site.register(Faculty)
admin.site.register(Profil)

Lancer le projet :
```py manage.py runserver```
```python manage.py runserver```

Migrations : 
```py manage.py makemigrations myapp```
```python manage.py makemigrations myapp```

Launch migration : 
```py manage.py migrate```
```python manage.py migrate```

Le shell de python :
```py manage.py shell```
```python manage.py shell```

exemple de test sur le shell pour créer une promotion :

```
>>> from myapp.models import Promotion
>>> Promotion.objects.all()
>>> promotion = Promotion()
>>> promotion.save()
>>> promotion.id
```

Créer l'admin pour l'interface admin :
```py manage.py createsuperuser```
```python manage.py createsuperuser```

-Modifier le fichier admin.py pour avoir les models sur l'interface admin
